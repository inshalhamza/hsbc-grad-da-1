package com.hsbc.currentAccountManagement.model;

public class CurrentAccount {
	private long accountNumber;
	private String accountName;
	private static int counter = 100;
	private long contactNumber;
	private double accountBalance;
	
	public CurrentAccount(String accountName, long contactNumber, double accountBalance) {
		this.accountNumber = ++ counter;
		this.accountName = accountName;
		this.contactNumber = contactNumber;
		this.accountBalance = accountBalance;
	}
	@Override
	public String toString() {
		return "CurrentAccount [accountNumber=" + accountNumber + ", accountName=" + accountName + ", contactNumber="
				+ contactNumber + ", accountBalance=" + accountBalance + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountName == null) ? 0 : accountName.hashCode());
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrentAccount other = (CurrentAccount) obj;
		if (accountName == null) {
			if (other.accountName != null)
				return false;
		} else if (!accountName.equals(other.accountName))
			return false;
		if (accountNumber != other.accountNumber)
			return false;
		return true;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	

}
