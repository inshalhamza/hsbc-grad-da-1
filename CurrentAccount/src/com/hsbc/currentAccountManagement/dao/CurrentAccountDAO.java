package com.hsbc.currentAccountManagement.dao;

import java.util.List;

import com.hsbc.currentAccountManagement.exception.AccountNotFoundException;
import com.hsbc.currentAccountManagement.model.CurrentAccount;

public interface CurrentAccountDAO {
	
	CurrentAccount storeCurrentAccount(CurrentAccount account);
	void deleteCurrentAccount(long accountNumber);
	List<CurrentAccount> fetchCurrentAccounts();
	CurrentAccount fetchCurrentAccountByAccountNumber(long accountNumber)  throws AccountNotFoundException;
	 CurrentAccount updateCurrentAccount(long accountNumber,CurrentAccount currentAccount)  ;

}
