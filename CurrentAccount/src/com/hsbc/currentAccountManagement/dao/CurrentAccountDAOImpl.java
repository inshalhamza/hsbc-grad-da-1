package com.hsbc.currentAccountManagement.dao;

import java.util.ArrayList;
import java.util.List;

import com.hsbc.currentAccountManagement.exception.AccountNotFoundException;
import com.hsbc.currentAccountManagement.model.CurrentAccount;


public class CurrentAccountDAOImpl implements CurrentAccountDAO {
	
	private static List<CurrentAccount> currentAccountsList = new ArrayList<>(); 

	@Override
	public CurrentAccount storeCurrentAccount(CurrentAccount account) {
		currentAccountsList.add(account);
		return account;
	}

	@Override
	public void deleteCurrentAccount(long accountNumber) {
		for (CurrentAccount currentAccount : currentAccountsList) {
			if (currentAccount.getAccountNumber() == accountNumber) {
				currentAccount = null;
			}
		}
		
	}

	@Override
	public List<CurrentAccount> fetchCurrentAccounts() {
		return currentAccountsList;
	}

	@Override
	public CurrentAccount fetchCurrentAccountByAccountNumber(long accountNumber) throws AccountNotFoundException{
		for (CurrentAccount currentAccount : currentAccountsList) {
			if (currentAccount.getAccountNumber() == accountNumber) {
				return currentAccount;
			}
		}
		throw new AccountNotFoundException("Account Does Not Exist ");
	}

	public CurrentAccount updateCurrentAccount(long accountNumber,CurrentAccount currentAccount) {
		for (CurrentAccount sa :  currentAccountsList) {
			if(sa.getAccountNumber() == accountNumber) {
				sa = currentAccount;
			}
		}
		return currentAccount;
	}

}
