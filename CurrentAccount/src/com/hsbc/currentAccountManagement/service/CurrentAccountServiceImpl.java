package com.hsbc.currentAccountManagement.service;

import java.util.List;

import com.hsbc.currentAccountManagement.dao.CurrentAccountDAO;
import com.hsbc.currentAccountManagement.dao.CurrentAccountDAOImpl;
import com.hsbc.currentAccountManagement.exception.AccountNotFoundException;
import com.hsbc.currentAccountManagement.exception.InsufficientBankResourcesException;
import com.hsbc.currentAccountManagement.model.CurrentAccount;

public  class CurrentAccountServiceImpl implements CurrentAccountService{
	private CurrentAccountDAO dao = new CurrentAccountDAOImpl(); 

	@Override
	public CurrentAccount createAccount(String accountName, long contactNumber, double accountBalance) {
		CurrentAccount currentAccount = new CurrentAccount(accountName, contactNumber, accountBalance);
		CurrentAccount currentAccountCreated = this.dao.storeCurrentAccount(currentAccount);
		return currentAccountCreated;
	}

	@Override
	public void closeAccount(long accountNumber) {
		this.dao.deleteCurrentAccount(accountNumber);
		
	}

	@Override
	public long withdrawAmount(long accountNumber, long amount)  throws AccountNotFoundException{
		CurrentAccount currentAccount = this.dao.fetchCurrentAccountByAccountNumber(accountNumber);
		if(currentAccount != null) {
			double currentAccountBalance = currentAccount.getAccountBalance() ;
			if(currentAccountBalance>amount) {
				double updatedBalance = currentAccountBalance - amount;
				currentAccount.setAccountBalance(updatedBalance);
				this.dao.updateCurrentAccount(accountNumber,currentAccount);
				return amount;
			}
			
		}
		return 0;
	}

	@Override
	public void depositAmount(long accountNumber, long amount)  throws AccountNotFoundException{
		CurrentAccount currentAccount = this.dao.fetchCurrentAccountByAccountNumber(accountNumber);
		if(currentAccount != null) {
			double updatedBalance = currentAccount.getAccountBalance() + amount;
			currentAccount.setAccountBalance(updatedBalance);
			this.dao.updateCurrentAccount(accountNumber, currentAccount);
		}
	}

	@Override
	public List<CurrentAccount> fetchCurrentAccounts() {
		return this.dao.fetchCurrentAccounts();
	}

	@Override
	public CurrentAccount fetchCurrentAccountByAccountNumber(long accountNumber) throws AccountNotFoundException{
		return this.dao.fetchCurrentAccountByAccountNumber(accountNumber);
	}

	@Override
	public void transfer(long accountId, long toId, long amount) throws AccountNotFoundException,InsufficientBankResourcesException {
		if(this.dao.fetchCurrentAccountByAccountNumber(accountId).getAccountBalance() > amount) {
		long transferrableAmount = this.withdrawAmount(accountId, amount); 
		this.depositAmount(toId, transferrableAmount);
		}
		else {
			throw new InsufficientBankResourcesException("Does Not Have Enough Balance for Transfer for Client "+this.dao.fetchCurrentAccountByAccountNumber(accountId).getAccountName());
		}
		
		
	}

	

}
