package com.hsbc.currentAccountManagement.service;

import java.util.List;

import com.hsbc.currentAccountManagement.exception.AccountNotFoundException;
import com.hsbc.currentAccountManagement.exception.InsufficientBankResourcesException;
import com.hsbc.currentAccountManagement.model.CurrentAccount;

public interface CurrentAccountService {
	
	CurrentAccount createAccount(String accountName, long contactNumber, double accountBalance);
	void closeAccount(long accountNumber);
	long withdrawAmount(long accountNumber,long amount) throws AccountNotFoundException;
	void depositAmount(long accountNumber, long amount) throws AccountNotFoundException;
	List<CurrentAccount> fetchCurrentAccounts();
	CurrentAccount fetchCurrentAccountByAccountNumber(long accountNumber) throws AccountNotFoundException;
	void transfer(long accountId, long toId, long amount) throws AccountNotFoundException,InsufficientBankResourcesException;
	
	
	

}
