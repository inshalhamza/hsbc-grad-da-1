package com.hsbc.currentAccountManagement.exception;

public class AccountNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFoundException(String Message) {
		super(Message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	

}
