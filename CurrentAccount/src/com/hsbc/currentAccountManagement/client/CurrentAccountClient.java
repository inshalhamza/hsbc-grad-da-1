package com.hsbc.currentAccountManagement.client;

import com.hsbc.currentAccountManagement.exception.AccountNotFoundException;
import com.hsbc.currentAccountManagement.exception.InsufficientBankResourcesException;
import com.hsbc.currentAccountManagement.model.CurrentAccount;
import com.hsbc.currentAccountManagement.service.CurrentAccountService;
import com.hsbc.currentAccountManagement.service.CurrentAccountServiceImpl;

public class CurrentAccountClient {
	public static void main(String[] args) {
		 CurrentAccountService currentAccountService = new CurrentAccountServiceImpl();  
		 
		 CurrentAccount inshal = currentAccountService.createAccount("Inshal", 7338817396L, 56000);
		 CurrentAccount azli = currentAccountService.createAccount("Azli", 8604050418L, 60000);
		try { 
		 currentAccountService.depositAmount(1003, 10000);
		 currentAccountService.transfer(azli.getAccountNumber(), inshal.getAccountNumber(), 80000);
		}
		catch(AccountNotFoundException accountNotFound) {
			System.out.println(accountNotFound.getMessage());
		}
		catch(InsufficientBankResourcesException insufficientBankResources) {
			System.out.println(insufficientBankResources.getMessage());
		}
		
		System.out.println("-----------------Details of Current Account--------------------");
		 System.out.println("Inshal Account Details are: "+ inshal);
		 System.out.println("Inshal Account Details are: "+ azli);
		
	}

}
