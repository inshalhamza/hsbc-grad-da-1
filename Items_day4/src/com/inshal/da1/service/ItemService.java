package com.inshal.da1.service;

import com.inshal.da1.model.Items;

public interface ItemService {
	
	public Items saveItem(String itemName, long itemId, double itemPrice);
	public void deleteItem(long itemId);
	public Items fetchItemsById(long itemId);
	public Items[] fetchItems();
	public double finalAmount();
	

}
