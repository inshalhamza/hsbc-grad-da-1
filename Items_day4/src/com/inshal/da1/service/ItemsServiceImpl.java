package com.inshal.da1.service;


import com.inshal.da1.dao.ItemsDAO;
import com.inshal.da1.dao.ItemsDAOImpl;
import com.inshal.da1.model.Items;

public class ItemsServiceImpl implements ItemService 
{
	private double totalAmount = 0;
	
	ItemsDAO itemsDao = new ItemsDAOImpl();
		

	public Items saveItem(String itemName, long itemId, double itemPrice) {
		Items item = new Items(itemName,itemId,itemPrice);
		Items itemAdded = this.itemsDao.saveItem(item);
		return itemAdded;
	}


	public void deleteItem(long itemId) {
		this.itemsDao.deleteItems(itemId);
		
	}

	public Items fetchItemsById(long itemId) {
		return this.itemsDao.getItemsbyID(itemId);
		
	}

	public Items[] fetchItems() {
		
		return this.itemsDao.getItems();
	}



	public double finalAmount() {
		Items[] items = itemsDao.getItems();
		totalAmount = 0;
			for(Items item: items) {
			if(item != null) {
				totalAmount = totalAmount + item.getItemPrice();
			}
		}
		return 0;
	}

}
