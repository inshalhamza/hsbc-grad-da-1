package com.inshal.da1.model;

public class Items {
	
	private String itemName;
	private long itemId;
	private double itemPrice;
	public String getItemName() {
		return itemName;
	}
	
	public Items(String itemName, long itemId, double itemPrice) {
		this.itemName = itemName;
		this.itemId = itemId;
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "Items [itemName=" + itemName + ", itemId=" + itemId + ", itemPrice=" + itemPrice + "]";
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	

}
