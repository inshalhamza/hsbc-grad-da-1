package com.inshal.da1.client;

import com.inshal.da1.controller.ItemController;
import com.inshal.da1.model.Items;

public class ItemsClient {
	public static void main(String[] args) {
		
		ItemController itemController = new ItemController();
		
		Items cup = itemController.addItems("Cup", 011, 150);
		Items pens = itemController.addItems("Pens", 001, 15);
		Items laptops = itemController.addItems("Laptops", 101, 15);
		
		System.out.println("Item Id : "+cup.getItemId());
		System.out.println("Item Name : "+ cup.getItemName());
		System.out.println("Item Price : "+cup.getItemPrice());
		
		System.out.println("Item Id : "+laptops.getItemId());
		System.out.println("Item Name : "+ laptops.getItemName());
		System.out.println("Item Price : "+laptops.getItemPrice());
		
		System.out.println("Item Id : "+pens.getItemId());
		System.out.println("Item Name : "+ pens.getItemName());
		System.out.println("Item Price : "+pens.getItemPrice());
		
		System.out.println("----------------------------------------");
		
		Items[] items = itemController.fetchItems();
		
		for(Items item: items) {
			if ( item != null) {
				System.out.println("Item Id : "+item.getItemId());
				System.out.println("Item Name : "+ item.getItemName());
				System.out.println("Item Price : "+item.getItemPrice());
			
				
				
			}
		}
		
		System.out.println("FinalAmount: " +itemController.finalAmount());
		
	}

}
