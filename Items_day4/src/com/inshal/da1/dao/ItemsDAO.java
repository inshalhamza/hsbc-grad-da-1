package com.inshal.da1.dao;


import com.inshal.da1.model.Items;

public interface ItemsDAO {
	
	public Items saveItem(Items item);
	public void deleteItems(long itemId);
	public Items getItemsbyID(long itemId);
	public Items[] getItems();
	public Items updateItems(long itemid,Items item);
	

}
