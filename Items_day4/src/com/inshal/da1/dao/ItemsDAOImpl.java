package com.inshal.da1.dao;



import com.inshal.da1.model.Items;

public class ItemsDAOImpl implements ItemsDAO{
	private static Items[] items = new Items[100];
	private static int counter = 0;

	public Items saveItem(Items item) {
		items[++counter] = item;
		
		return item;
	}

	public void deleteItems(long itemId) {
		for(int index = 0; index < items.length ; index ++) {
			if(items[index].getItemId() == itemId) {
				items[index] = null;				
			}
		}
	}


	public Items getItemsbyID(long itemId) {
		for (Items item: items) {
			if(item.getItemId() == itemId) {
				return item;
			}
		}
		return null;
	}

	
	public Items[] getItems() {
		return items;
	}

	
	public Items updateItems(long itemid, Items item) {
		for(int index = 0; index < items.length ; index ++) {
			if(items[index].getItemId() == itemid) {
				items[index] =item;
				break;
				
			}
		}
		return item;
	}
	


}
