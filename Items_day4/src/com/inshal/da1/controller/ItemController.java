package com.inshal.da1.controller;

import com.inshal.da1.model.Items;
import com.inshal.da1.service.ItemService;
import com.inshal.da1.service.ItemsServiceImpl;

public class ItemController {
	
	private ItemService itemService= new ItemsServiceImpl();
	
	public Items addItems(String itemName, long itemId, double itemPrice) {
		Items item=this.itemService.saveItem(itemName, itemId, itemPrice);
		return item;
	}
	
	public void deleteItem(long itemId) {
		this.itemService.deleteItem(itemId);
		
	}
	
	public Items fetchItemsById(long itemId) {
		return this.itemService.fetchItemsById(itemId);
		
	}
	public Items[] fetchItems() {
		
		return this.itemService.fetchItems();
	}

	public double finalAmount() {
		return this.itemService.finalAmount();

	}

}
