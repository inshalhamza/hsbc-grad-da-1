package com.inshal.da1.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import com.inshal.da1.model.Item;
import com.inshal.da1.service.ItemService;
import com.inshal.da1.service.ItemServiceImpl;

public class ItemsClient {

	public static void main(String[] args) {

		ItemService itemServiceController = new ItemServiceImpl();
		itemServiceController.storeItems("car", 150);
		itemServiceController.storeItems("pens", 15);
		itemServiceController.storeItems("Folders", 200);
		itemServiceController.storeItems("PowerBanks", 15000);
		
		List<Item> it = itemServiceController.fetchAllItems();
		
		List<Item> deserialList = new ArrayList<>();
		
		// Serialization of contents.
		try {
			FileOutputStream fileOut = new FileOutputStream("C://Users//insha//Desktop//file.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(it);
			out.close();
			fileOut.close();
			System.out.println("\nSerialization Successful... Checkout your specified output file..\n");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Deserialization of contents.
		try {
			FileInputStream fileIn = new FileInputStream("C://Users//insha//Desktop//file.txt");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			System.out.println("Deserialized Data: \n");
			 deserialList =   (List<Item>) in.readObject();
			in.close();
			fileIn.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

			for(Item l:  deserialList) {
				System.out.println("items: "+ l);
			}
	}
}
