package com.inshal.da1.service;

import java.util.List;

import com.inshal.da1.dao.ItemsDao;
import com.inshal.da1.model.Item;
import com.inshal.da1.model.ItemsDAOImpl;

public class ItemServiceImpl implements ItemService {
	private ItemsDao dao = new ItemsDAOImpl();
	@Override
	public Item storeItems(String itemName,long itemPrice) {
		Item itemSa = new Item(itemName, itemPrice);
		Item itemSaved= this.dao.storeItems(itemSa);
		return itemSaved;
	}

	@Override
	public List<Item> fetchAllItems() {
		List<Item> items = this.dao.fetchAllItems();
		return items;
	}

}
