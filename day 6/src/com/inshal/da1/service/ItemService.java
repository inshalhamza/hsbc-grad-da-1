package com.inshal.da1.service;

import java.util.List;

import com.inshal.da1.model.Item;

public interface ItemService {
	
	Item storeItems(String itemName,long itemPrice);

	List<Item> fetchAllItems();

}
