package com.inshal.da1.dao;

import java.util.List;

import com.inshal.da1.model.Item;

public interface ItemsDao {
	
	Item storeItems(Item item);
	
	List<Item> fetchAllItems();
	

}
