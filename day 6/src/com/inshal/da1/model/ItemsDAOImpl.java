package com.inshal.da1.model;


import java.util.ArrayList;
import java.util.List;

import com.inshal.da1.dao.ItemsDao;

public class ItemsDAOImpl implements ItemsDao {
	
	private List<Item> items = new ArrayList<Item>();

	@Override
	public Item storeItems(Item item) {
		items.add(item);
		return item;
	}

	@Override
	public List<Item> fetchAllItems() {
		return items;
	}
	
	

}
