package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.model.Item;

public interface ItemService {

	static final ItemService itemService = new ItemServiceImpl();
	Item saveItem(Item item);

	List<Item> fetchItems();

	static ItemService getItemService() {
		return new ItemServiceImpl();
	}
}
