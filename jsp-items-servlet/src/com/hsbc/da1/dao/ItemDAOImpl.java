package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;

import com.hsbc.da1.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static final String INSERT_ITEM = "insert into items (itemname, price ) values (?,?)";
	private static final String FETCH_ITEMS = "select * from items";

	//private static List<Item> items = new ArrayList<>();

	@Override
	public Item saveItem(Item item) {
		Connection dbConnection = getConnection();
		if (dbConnection != null) {
			System.out.println("Connected");
		}
		try {
			PreparedStatement pStmt = dbConnection.prepareStatement(INSERT_ITEM);
			pStmt.setString(1, item.getName());
			pStmt.setDouble(2, item.getPrice());
			int rows = pStmt.executeUpdate();
			if (rows == 1) {
				return item;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		//items.add(item);
		System.out.println(" Came inside the dao method");
//		System.out.println(items);
		return item;
	}

	@Override
	public List<Item> fetchItems() {
		Connection dbConnection = getConnection();
		List<Item> items = new ArrayList<>();
		try {
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(FETCH_ITEMS);
			while (rs.next()) {
				long itemId = rs.getLong("itemid");
				String name = rs.getString("itemname");
				double price = rs.getDouble("price");
				items.add(new Item(itemId, name, price));			
			}
			System.out.println("Reached fetch");
			return items;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return this.items;
		return null;
	}

	private Connection getConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			return DriverManager.getConnection("jdbc:derby://localhost:1527/items", "admin", "password");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return null;
	}

}
