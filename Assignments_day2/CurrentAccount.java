/**
 * This program is for Current Account.
 * Create a Current Account class
    * Attributes

        * Account Number - M
        * Account balance - O
        * GST Number - M
        * Customer Name - M
        * Business Name - M
        * Communication Address - O
    * There is no withdrawal limit
    * There is no deposit limit
    * Minimum account balance should be 50_000
    * All customers should have unique account number.
    * The account number should be incremental starting with 10_000
    * The customer cannot update his GST Number and Business name but can update communication address
 * @ Inshal Hamza
 * @ Version: 1
 * @ Date: 9/16/2020
*/

public class CurrentAccount {

    //static variables 
    private static long accountNumberTracker = 10_000;
    private static Boolean acCreation;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String customerName;

    private Address address;

    private String businessName;
    

    private String GSTNumber;

    public String getAccountNumber() {
        if (acCreation == true){
            return Long.toString(accountNumber);
        }
        else{ 
            return "Account not created due to minimum balance requirement not met.\n";
        }
        
        
    }

    public void updateAddress(Address address) {
        this.address = address;   
        
    }

    public Address getAddress(Address address) {
        return this.address;}
    


    
    public CurrentAccount(String customerName, double initialAccountBalance,String businessName,String GSTNumber) 
    {   
        
        if (initialAccountBalance > 50000){
            this.customerName = customerName;
            this.accountBalance = initialAccountBalance;
            this.businessName = businessName;
            this.GSTNumber = GSTNumber;
            this.accountNumber = ++ accountNumberTracker;
            acCreation = true;
            
        }
        else{
            acCreation = false;
            System.out.println("Please Have mininmum Balance of : 50_000 to Create an Account.");
        }
    }

    public CurrentAccount(String customerName, double initialAccountBalance,String businessName,String GSTNumber,Address address) 
    {   
        
        if (initialAccountBalance > 50000){
            this.customerName = customerName;
            this.accountBalance = initialAccountBalance;
            this.businessName = businessName;
            this.GSTNumber = GSTNumber;
            this.accountNumber = ++ accountNumberTracker;
            acCreation = true;
            this.address = address;
        }
        else{
            acCreation = false;
            System.out.println("Please Have mininmum Balance of : 50_000 to Create an Account.");
        }
    }
    
    // instance methods
    public double withdraw(double amount) {
        
            this.accountBalance = this.accountBalance - amount;
            return amount;
        
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    // public void commAddress(String streetName, int zipCode, String city) {

    //     this.street = streetName;
    //     this.zipCode = zipCode;
    //     this.city = city;
    //     System.out.println("The Updates Details are :");
    //     System.out.println(streetName);
    //     System.out.println(city);
    //     System.out.println(zipCode);
    // }

    // public void transferAmount(double amount, long userAccountId){
    //     //validation
    //     CurrentAccount userAccount = CurrentAccountRegister.fetchCurrentAccountByAccountId(userAccountId);
    //     if(userAccount != null){
    //         if(validateAmount(amount)){
    //         //deduct the amount
    //             this.accountBalance-=amount;
    //             userAccount.accountBalance+=amount;
    //             System.out.println("The transfer completed.");
    //         //deposit to user's account
    //         }
    //     }
    // }

    public String getCustomerName() {
        return this.customerName;
    }

    public Address getAddressName(Address address) {
        return address;
    }

    public String getBussinessName() {
        return this.businessName;
    }

    public String getGSTsName() {
        return this.GSTNumber;
    }

    

}