/**
 * This is a Main Class for CurrentAccount class which is having has a relation on Address class.
 * @ Inshal Hamza
 * @ Date: 9/16/2020
 */
public class CurrentAccountClient{

    public static void main(String[] args) {


        System.out.println(" ---------------------------------");

        CurrentAccount naveen = new CurrentAccount("Naveen", 52_000, "ImportExport","123$GN45");
        System.out.println("Account Number " + naveen.getAccountNumber());
        System.out.println("Customer Name: " + naveen.getCustomerName());
        System.out.println("Initial Account balance " + naveen.checkBalance());
        System.out.println("Account Balance " + naveen.deposit(4000));
        System.out.println("Balance after deposit" + naveen.checkBalance());
        naveen.withdraw(2000);
        System.out.println("Balance after Withdrawal " + naveen.checkBalance());
        System.out.println("Bussiness Name is: " + naveen.getBussinessName());
        System.out.println("Gst Number is: " + naveen.getGSTsName());

        System.out.println(" ---------------------------------");

        CurrentAccount Inshal = new CurrentAccount("Inshal", 49_000, "ImportExport","123$GN45");
        System.out.println("Account Number " + Inshal.getAccountNumber());
        
        System.out.println("----------------------------------");
        Address address = new Address("19/505","Kanpur","Uttar Pradesh",208012);
        CurrentAccount rishab = new CurrentAccount("Rishab", 60_000, "ImportExport","12345HK",address);
        System.out.println("Customer Name: " + rishab.getCustomerName());
        System.out.println("City is :"+rishab.getAddressName(address).getCity());
        System.out.println("Street is: "+rishab.getAddressName(address).getStreet());
        System.out.println("State is: "+rishab.getAddressName(address).getState());

        System.out.println("Balance after Withdrawal " + rishab.checkBalance());
        System.out.println("Bussiness Name is: " + rishab.getBussinessName());
        System.out.println("Gst Number is: " + rishab.getGSTsName());



    }
} 