/**
 * In progress.
 * Needs some work
 * @ Inshal Hamza
 * @ Date: 9/16/2020
 */

public class CallRefVal {

    public static void main(String[] args) {

        int array[] = new int[] {30, 5, 10, 44};

        System.out.println("-------------------------------");
        System.out.println("Call by reference");
        System.out.println("Changes before the call.\n");
        for (int a : array) {
            System.out.println(a);
        }
        System.out.println("-------------------------------");
        callByRef(array);
        System.out.printf("Changes after the call.\n");
        for (int a : array) {
            System.out.println(a);
        }

        System.out.println("-------------------------------");
        System.out.println("Call by value");
        System.out.println("Changes before the call\n");
        for (int a : array) {
            System.out.println(a);
        }
        System.out.println("-------------------------------");
        callByValue(array);
        System.out.println("Changes after the call\n");
        for (int a : array) {
            System.out.println(a);
        }
    }

    private static void callByValue(int[] arr) {
        arr[0] = arr[0] + 88;
        arr[1] = arr[1] + 20;
        arr[2] = arr[2] + 12;
        arr[3] = arr[3] + 3;

        System.out.println("Local changes applied");

        for (int a : arr) {
            System.out.println(a);
        }
    }

    private static void callByRef(int[] arr) {
        System.out.println("Changes inside the method: \n");

        arr[0] = arr[0] + 20;
        arr[1] = arr[1] + 21;
        arr[2] = arr[2] + 22;
        arr[3] = arr[3] + 23;
        
        for (int a : arr) {
            System.out.println(a);
        }

    }
}
