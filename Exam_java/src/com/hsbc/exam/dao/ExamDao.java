package com.hsbc.exam.dao;

import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;

public class ExamDao {
	
	public interface SavingsAccountDAO{
		SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);

		SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);

		void deleteSavingsAccount(long accountNumber);

		List<SavingsAccount> fetchSavingsAccounts();

		SavingsAccount fetchSavingsAccountById(long accountNumber) throws CustomerNotFound;
	}

}
