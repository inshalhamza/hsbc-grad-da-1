abstract class BankAccounts {
    public static long accountNumberTracker = 1000;
    private double accountBalance;
    private final double accountNumber;

    public BankAccounts(double initialBalance) {
        this.accountBalance = initialBalance;
        this.accountNumber = ++accountNumberTracker;
    }

    public void withdrawal(double amount, double minBalance) {
        if(amount <= this.accountBalance - minBalance)
        {
            this.accountBalance = this.accountBalance - amount;
            System.out.println("Withdrawal successful. Account Balance : " + this.accountBalance);
        }
    }

    public abstract void withdrawal(double amount);
    public abstract void loanEligibilty();
}

final class CurrentBankAccount extends BankAccounts {
    private final double minBalance = 25000;
    private final String GSTIN;
    private final String businessName;

    public CurrentBankAccount(final String name, final String GST, final double initialBalance) {
        super(initialBalance);
        this.businessName = name;
        this.GSTIN = GST;
        System.out.println("Current Bank Account Belongs to : " + this.businessName);
    }

    public final void withdrawal(double amount) {
        super.withdrawal(amount, minBalance);
    }

    public void loanEligibilty() {
        System.out.println("Eligible for loans upto 25L");
    }
}

final class SavingsBankAccount extends BankAccounts {
    private final double minBalance = 10000;
    private final String customerName;

    public SavingsBankAccount(final double initialBalance,final String customerName) {
        super(initialBalance);
        this.customerName= customerName;
        System.out.println("Account Owner is : "+this.customerName);
    }

    public final void withdrawal(double amount) {
        super.withdrawal(amount, minBalance);
    }

    public void loanEligibilty() {
        System.out.println("Eligible for loans upto 5L");
    }
}

final class SalaryBankAccount extends BankAccounts {
    private final double minBalance = 0;
    private final String employeeName;

    public SalaryBankAccount(final double initialBalance,final String empName) {
        super(initialBalance);
        this.employeeName = empName;
        System.out.println("This Salary Account Belongs to : "+ this.employeeName);
    }

    public final void withdrawal(double amount)
    {
        super.withdrawal(amount, minBalance);
    }

    public void loanEligibilty() {
        System.out.println("Eligible for loans upto 10L");
    }
}

public final class BankAccountsClient {
    public static void main(final String[] args) {
        BankAccounts BankAccounts = null;
        final int BankAccountType = Integer.parseInt(args[0]);
        if(BankAccountType == 1) {
            BankAccounts = new CurrentBankAccount("BlackRock", "GSTNMBR123", 50000);
            BankAccounts.withdrawal(5000);
            BankAccounts.loanEligibilty();
        }            
        else if(BankAccountType == 2) {
            BankAccounts = new SavingsBankAccount(100000,"Inshal Hamza");
            BankAccounts.withdrawal(2000);
            BankAccounts.loanEligibilty();
        }
        else if(BankAccountType == 3) {
            BankAccounts = new SalaryBankAccount(50000,"HAMZA Inshal");
            BankAccounts.withdrawal(1000);
            BankAccounts.loanEligibilty();
        }
    }
}