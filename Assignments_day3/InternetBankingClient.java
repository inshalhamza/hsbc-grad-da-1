interface TransfertoBank {
    void transfer(int accNumber,String ifscCode,double amount, String notes );
}

interface PaymentGateway {
    
    void pay(String from, String to, double amount, String notes);

}

interface MobileRecharge {

    void rechargeMobile(String phonenumber, double amount);

}

interface ElectricBillRecharge {

    void electricRecharge(int connectionNumber, double amount,String phonenumber);

}




class GooglePay implements PaymentGateway, MobileRecharge,ElectricBillRecharge {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Google Pay");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + "with GooglePay ");
    }

    public void electricRecharge(int connectionNumber, double amount,String phonenumber){
        System.out.println("Electricity recharge for Connection No: " + connectionNumber + " of amount " + amount + "associated with Mobile Number: " +phonenumber+ " Done by GooglePay");
    }

}

class PhonePay implements PaymentGateway, MobileRecharge,ElectricBillRecharge {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Phone Pay");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " with PhonePay");
    }
    public void electricRecharge(int connectionNumber, double amount,String phonenumber){
        System.out.println("Electricity recharge for Connection No: " + connectionNumber + " of amount " + amount + "associated with Mobile Number: " +phonenumber+ " Done by GooglePay");
    }
}

class JioPay implements PaymentGateway {
    public void pay(String from, String to, double amount, String notes) {
        System.out.println(
                "Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes + " Using Jio Pay");
    }
    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " using JioPay");
    }

}

class AmazonPay implements PaymentGateway,MobileRecharge,TransfertoBank,ElectricBillRecharge{
    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from "+ from + " to: " + to + " of Amount " + amount + " Notes : " + notes + " Using AmazonPay");
    }
    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " with AmazonPay");
    }
    public void transfer(int accNumber,String ifscCode,double amount, String notes ){
        System.out.println("Transfered Money To Bank Account no: " + accNumber + " with IFSC code: " + ifscCode +" Credited amount: "+ amount + " with Amazon Pay");
    }
    public void electricRecharge(int connectionNumber, double amount,String phonenumber){
        System.out.println("Electricity recharge for Connection No: " + connectionNumber + " of amount " + amount + "associated with Mobile Number: " +phonenumber+ " Done by AmazonPay");
    }

}

public class InternetBankingClient {
    public static void main(String[] args) {

        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;
        TransfertoBank transferBank = null;
        ElectricBillRecharge electricpayment = null; 
        if (args[0] == "1") {
            GooglePay googlePay = new GooglePay();
            paymentGateway = googlePay;
            mobileRecharge = googlePay;
            electricpayment = googlePay;
        } else if (args[0] == "2") {
            PhonePay phonePay = new PhonePay();
            paymentGateway = phonePay;
            mobileRecharge = phonePay;
            electricpayment = phonePay;
        } else if (args[0] == "3") {
            JioPay jioPay = new JioPay();
            paymentGateway = jioPay;
        } else {
            AmazonPay amazonPay = new AmazonPay();
            paymentGateway = amazonPay;
            mobileRecharge = amazonPay;
            transferBank = amazonPay;
            electricpayment = amazonPay;

        }
        paymentGateway.pay("Nikhil", "Kiran", 15_000, "Please confirm");
        mobileRecharge.rechargeMobile("9877784578", 250);
        transferBank.transfer(616198366,"SBIN0007862",1_00_000,"Amount Transfer Initiated please Confirm.");
        electricpayment.electricRecharge(321456, 1258,"7338817396");
    }
}