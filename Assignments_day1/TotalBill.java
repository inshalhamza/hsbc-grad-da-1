/**
 * This program is for Question.
 * Accept a total bill in decimal format and print the total bill amount after applying 15% tax.
 * @    InshalHamza
 * @    Version: 1
 * @    Date: 9/15/2020
 */
public class TotalBill{
    public static void main(String[] args) {
        String billAmount = args[0];
        //Converting bill to double with 15% interest.
        double valueInDecimal = Float.valueOf(billAmount)*1.15;
        System.out.println("Final amount is :  "+ valueInDecimal);
    }
}