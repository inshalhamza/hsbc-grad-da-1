/**
 * This program is for Question.
 * Accept 5 integer values as input and print the min and the max value.
 * @    InshalHamza
 * @    Version: 1
 * @    Date: 9/15/2020
 */

public class Minmax{
    public static void main(String[] args) {
        int[] minmax = new int[5];
        
        for (int i=0;i<args.length;i++)
            minmax[i]=(Integer.parseInt(args[i]));
        int max_number=minmax[0] ;
        
        // for max_number
        for (int i : minmax){
            if (i>max_number){
                max_number=i;
            }
        }
        int min_number=minmax[minmax.length-1];
        // for min_number
        for (int i=minmax.length-1;i>=0;i--){
            if (minmax[i]<min_number){
                min_number=minmax[i];
            }
        }
       
        System.out.println("Max number is: "+max_number +" Min number is: "+min_number);
    }  
}
