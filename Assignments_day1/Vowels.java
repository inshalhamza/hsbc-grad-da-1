/**
 * This program is for Question.
 * Accept a letter and print if it is a vowel or a consonant.
 * @    InshalHamza
 * @    Version: 1
 * @    Date: 9/15/2020
 */

public class Vowels {

    public static void main(String[] args) {

        String val = args[0].toLowerCase();
        //Switch case for handling values for vowels
        switch (val) {
            case "a":
            case "u":
            case "o":
            case "i":
            case "e":
                System.out.println(" Its a Vowel !!");
                break;
            default:
                System.out.println(" Its a Consonant!!");
                break;
        }
        
    }
}