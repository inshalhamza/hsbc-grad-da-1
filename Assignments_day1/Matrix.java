/**
 * This program is for Question:
 * Create a two dimensional matrix of 5 X 5 and initialize the values starting with 10. 
 * Also print the values in a matrix format.
 * @   InshalHamza
 * @   version: 1
 * @   Date: 9/15/2020
 */

public class Matrix {
    public static void main(String[] args) {
        int [][] matrix = new int[5][5];
        int val=10;
        //for initilising 5 x 5 matrix with value starting from 10. 
        for (int row = 0 ; row<5 ; row++ ){
            for(int col = 0 ; col<5 ; col++ ){
                matrix[row][col] = val;
            }
            val=val+1;
        }
        
        //for printing 2-D array as Matrix
        for (int row = 0 ; row<5 ; row++ ){
            
            for(int col = 0 ; col <5 ; col++ ){
                System.out.print("      "+matrix[row][col]+" ");
            }
            System.out.println(" ");
        }

    }
        
}

