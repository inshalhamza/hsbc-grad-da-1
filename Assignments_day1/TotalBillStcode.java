/**
 * This program is for Question.
 * Accept the total bill in decimal format and the state code as second argument. 
 * Calculate the total bill based on the state code tax:
 *  1.  KA - 15%
 *  2.  TN - 18%
 *  3.  MH - 20%
 *  4.  OTHERS - 12%
 * @    InshalHamza
 * @    Version: 1
 * @    Date: 9/15/2020
 */
public class TotalBillStcode{
    public static void main(String[] args) {
        String billAmount = args[0];
        String StCode = args[1].toLowerCase();
        double valueInDecimal = Float.valueOf(billAmount);
        switch (StCode){
            case "ka": valueInDecimal = valueInDecimal * 1.15;    
            System.out.println("State Code = " + StCode.toUpperCase()+", Final amount = "+ valueInDecimal);
                break; 
            case "tn": valueInDecimal = valueInDecimal * 1.18;
                System.out.println("State Code = " + StCode.toUpperCase()+", Final amount = "+ valueInDecimal);
                break;
            case "mh": valueInDecimal = valueInDecimal * 1.20;
                System.out.println("State Code = " + StCode.toUpperCase()+", Final amount = "+ valueInDecimal);
                break;
            default : valueInDecimal = valueInDecimal * 1.12;
            System.out.println("State Code = " + StCode.toUpperCase()+", Final amount =  "+ valueInDecimal);
            break;
        }
    }
}