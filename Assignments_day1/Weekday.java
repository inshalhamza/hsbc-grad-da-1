/** 
 * This program is for Question.
 * Create a Java program to accept a day and print if it is a Weekday or a Weekend.
 * @    InshalHamza
 * @    Version : 1
 * @    Date: 9/15/2020
*/
public class Weekday {
    public static void main(String[] args) {
        String day = args[0].toUpperCase();
        switch (day) {
            case "SUNDAY":
                System.out.println(" Its a Weekend !!");
                break;
            case "MONDAY":
                System.out.println(" Its a Weekday !!");
                break;
            case "TUESDAY":
                System.out.println(" Its a Weekday !!");
                break;
            case "WEDNESDAY":
                System.out.println(" Its a Weekday !!");
                break;
            case "THURSDAY":
                System.out.println(" Its a Weekday !!");
                break;
            case "FRIDAY":
                System.out.println(" Its a Weekday !!");
                break;
            case "SATURDAY":
                System.out.println(" Its a Weekend !!");
                break;
            default:
                System.out.println(" Have a great Day !!");
                break;
        }
    }
}