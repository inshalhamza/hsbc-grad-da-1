package com.hsbc.da1.ageFromDOB;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


public class DobConversion {
	public static void main(String[] args) {
		System.out.print("Please enter date of birth in YYYY-MM-DD: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		System.out.println("\nDate of Birth: " + input);
		
		
		
		LocalDate pdate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));

		
		
		LocalDate now = LocalDate.now();
		Period diff =Period.between(pdate, now);
		
		System.out.println(pdate);
		System.out.println(now);
		System.out.println(diff);

		System.out.printf("\nI am  %d years, %d months and %d days old.\n\n", diff.getYears(), diff.getMonths(),
				diff.getDays());
		System.out.println("I was born on Day: " + pdate.getDayOfWeek()+" month: "+pdate.getMonth()+" date: "+pdate.getDayOfMonth()+" year: "+pdate.getYear());
		
	}
}