package com.hsbc.EmpMgmt.client;



import com.hsbc.EmpMgmt.controller.EmployeeDataController;
import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.exception.InsufficientLeaveBalanceException;
import com.hsbc.EmpMgmt.model.EmployeesData;


public class EmployeeDataClient {

		public static void main(String[] args) {
			
			EmployeeDataController empController = new EmployeeDataController();
			
			EmployeesData inshal = empController.addEmployee("inshal", 59000, 28);
			
								
			try {
				
			System.out.println("inshal account id --> "+inshal.getEmployeeId() );
			System.out.println("total number of Accounts --> "+empController.fetchAllEmployeesRecord().size());
			System.out.println(empController.fetchEmployeeDataById(1004));
			System.out.println(empController.fetchEmployeeDataByName("inshal"));
			System.out.println(empController.fetchAllEmployeesRecord());
			empController.applyForLeave(inshal.getEmployeeId(), 11);
			
			
			}
			catch(EmployeeNotFoundException employeeNotFound){
				
				System.out.println(employeeNotFound.getMessage());
			
			}catch(InsufficientLeaveBalanceException insufficientBalance) {
			
				System.out.println(insufficientBalance.getMessage());
			
			}

			
			System.out.println("------");
}}
