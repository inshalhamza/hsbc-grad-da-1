package com.hsbc.EmpMgmt.dao;

import java.util.ArrayList;
import java.util.List;

import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.model.EmployeesData;


public class ListBackedEmployeeDataDAOImpl  implements EmployeesDataDao{
	
	private static  List<EmployeesData> employeesDataList = new ArrayList<>();

	@Override
	public EmployeesData saveEmployeeData(EmployeesData employeeData) {
		employeesDataList.add(employeeData);
		return employeeData;
	}

	@Override
	public EmployeesData updateEmployeeData(long employeeId, EmployeesData employeeData) {
		for (EmployeesData employeData : employeesDataList) {
			if(employeData.getEmployeeId() == employeeId) {
				employeData = employeeData;
				
			}
		}
		return employeeData;
	}

	@Override
	public void deleteEmployeeData(long employeeId) {
		for (EmployeesData employeData : employeesDataList) {
			if(employeData.getEmployeeId() == employeeId) {
				employeData = null;
			}
		}
	}

	@Override
	public List<EmployeesData> fetchEmployeeData() {
		return employeesDataList;
	}

	@Override
	public EmployeesData fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException{
		for (EmployeesData employeData : employeesDataList) {
			if(employeData.getEmployeeId() == employeeId) {
					return employeData;
				}
			}
		throw new EmployeeNotFoundException("Employee doesnot exist");
	}
	

	@Override
	public EmployeesData fetchEmployeeDataByName(String employeeName) throws EmployeeNotFoundException {
		for (EmployeesData employeData : employeesDataList) {
			if(employeData.getEmployeeName() == employeeName ) {
					return employeData;
				}
			}
		throw new EmployeeNotFoundException("Employee doesnot exist");
	}

}
