package com.hsbc.EmpMgmt.dao;

import java.util.List;

import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.model.EmployeesData;


public interface EmployeesDataDao {
	
	EmployeesData saveEmployeeData(EmployeesData employeeData);

	EmployeesData updateEmployeeData(long employeeId, EmployeesData employeeData);

	void deleteEmployeeData(long employeeId);

	List<EmployeesData> fetchEmployeeData();

	EmployeesData fetchEmployeeDataById(long employeeId)  throws EmployeeNotFoundException ;

	EmployeesData fetchEmployeeDataByName(String employeeName)  throws EmployeeNotFoundException ;


}
