package com.hsbc.EmpMgmt.model;

public class EmployeesData implements Comparable< EmployeesData >  {
	private String employeeName;
	private long employeeId;
	private static int counter = 1000;
	private long employeeSalary;
	private int employeeAge;
	private long employeeLeaveBalance=40;
	private final long MIN_LEAVE_PER_APPLY = 10;
	
	
	public long getEmployeeLeaveLimit() {
		return employeeLeaveBalance;
	}
	public EmployeesData(String employeeName, long employeeSalary, int employeeAge) {
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.employeeAge = employeeAge;
		this.employeeId = ++ counter;
		this.employeeLeaveBalance = 40;
	}
	
	public final long getMinLeavePerApply() {
		return MIN_LEAVE_PER_APPLY;
	}
	@Override
	public String toString() {
		return "EmployeesData [employeeName=" + employeeName + ", employeeId=" + employeeId + ", employeeSalary="
				+ employeeSalary + ", employeeAge=" + employeeAge + ", employeeLeaveLimit=" +employeeLeaveBalance + "]";
	}
	public void setEmployeeLeaveLimit(long employeeLeaveLimit) {
		this.employeeLeaveBalance = employeeLeaveLimit;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public long getEmployeeSalary() {
		return employeeSalary;
	}
	public void setEmployeeSalary(long employeeSalary) {
		this.employeeSalary = employeeSalary;
	}
	public int getEmployeeAge() {
		return employeeAge;
	}
	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}
	
	@Override
	public int compareTo(EmployeesData o) {
		return (int) (this.employeeId - o.employeeId);
		
	}
	

}
