package com.hsbc.EmpMgmt.service;

import java.util.List;

import com.hsbc.EmpMgmt.dao.EmployeesDataDao;
import com.hsbc.EmpMgmt.dao.ListBackedEmployeeDataDAOImpl;
import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.exception.InsufficientLeaveBalanceException;
import com.hsbc.EmpMgmt.model.EmployeesData;

public class EmployeeDataServiceImpl  implements EmployeeDataService{

		private EmployeesDataDao dao = new ListBackedEmployeeDataDAOImpl();
		
		
	@Override
	public EmployeesData addEmployee(String employeeName, long employeeSalary, int employeeAge) {
		EmployeesData employeeData = new EmployeesData(employeeName, employeeSalary, employeeAge) ;
		EmployeesData employeeDataCreated = this.dao.saveEmployeeData(employeeData);
		return employeeDataCreated;
	}

	@Override
	public void removeEmployee(long employeeId) {
		this.dao.deleteEmployeeData(employeeId);
		
	}

	@Override
	public List<EmployeesData> fetchAllEmployeesRecord() {
		List<EmployeesData> listEmployee = this.dao.fetchEmployeeData();
		return listEmployee ;
	}

	@Override
	public EmployeesData fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException {
		EmployeesData employeeData = this.dao.fetchEmployeeDataById(employeeId);
		return employeeData;
	}

	@Override
	public EmployeesData fetchEmployeeDataByName(String employeeName)  throws EmployeeNotFoundException {
		EmployeesData employeeData = this.dao.fetchEmployeeDataByName(employeeName);
		return employeeData;
	}

	@Override
	public void applyForLeave(long employeeId, long days)  throws EmployeeNotFoundException ,InsufficientLeaveBalanceException {
		EmployeesData employeeData = this.dao.fetchEmployeeDataById(employeeId);
		
		if (days > employeeData.getMinLeavePerApply()) {
			System.out.println("Hi " + employeeData.getEmployeeName() + "! You can apply leave for maximum 10 days");
		}
		else if (employeeData.getEmployeeLeaveLimit() >= days) {
			employeeData.setEmployeeLeaveLimit((employeeData.getEmployeeLeaveLimit() - days));
			System.out.println("Your left leaves are: "+employeeData.getEmployeeLeaveLimit());
		}
		else {
			throw new InsufficientLeaveBalanceException("You Do not Have Enough leaves.");
		}
	}

	

}
