package com.hsbc.EmpMgmt.service;

import java.util.List;

import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.exception.InsufficientLeaveBalanceException;
import com.hsbc.EmpMgmt.model.EmployeesData;



public interface EmployeeDataService {
	
	public EmployeesData addEmployee(String employeeName, long employeeSalary, int employeeAge);

	public void removeEmployee(long employeeId);

	
	public List<EmployeesData> fetchAllEmployeesRecord();

	public EmployeesData fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException ;
	
	public EmployeesData fetchEmployeeDataByName(String employeeName) throws EmployeeNotFoundException  ;


	public void applyForLeave(long employeeId,long days) throws EmployeeNotFoundException ,InsufficientLeaveBalanceException;

	

}
