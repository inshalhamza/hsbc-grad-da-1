package com.hsbc.EmpMgmt.exception;

public class InsufficientLeaveBalanceException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientLeaveBalanceException(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	

}
