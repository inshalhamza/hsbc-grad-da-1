package com.hsbc.EmpMgmt.controller;

import java.util.List;

import com.hsbc.EmpMgmt.exception.EmployeeNotFoundException;
import com.hsbc.EmpMgmt.exception.InsufficientLeaveBalanceException;
import com.hsbc.EmpMgmt.model.EmployeesData;
import com.hsbc.EmpMgmt.service.EmployeeDataService;
import com.hsbc.EmpMgmt.service.EmployeeDataServiceImpl;

public class EmployeeDataController {
	
	private EmployeeDataService employeeService = new EmployeeDataServiceImpl();
	
	public EmployeesData addEmployee(String employeeName, long employeeSalary, int employeeAge) {
		EmployeesData employee = this.employeeService.addEmployee(employeeName, employeeSalary, employeeAge);
		return employee;
		
	}

	public void removeEmployee(long employeeId) {
		this.employeeService.removeEmployee(employeeId);
	}

	
	public List<EmployeesData> fetchAllEmployeesRecord(){
		List<EmployeesData> listEmployee = this.employeeService.fetchAllEmployeesRecord();
		return listEmployee;
	}

	public EmployeesData fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException {
		EmployeesData employee = this.employeeService.fetchEmployeeDataById(employeeId);
		return  employee;
	}
	
	public EmployeesData fetchEmployeeDataByName(String employeeName)  throws EmployeeNotFoundException {
		EmployeesData employee = this.employeeService.fetchEmployeeDataByName(employeeName);
		return employee;
	}


	public void applyForLeave(long employeeId,long days) throws EmployeeNotFoundException,InsufficientLeaveBalanceException {
		this.employeeService.applyForLeave(employeeId, days);
	}

}
