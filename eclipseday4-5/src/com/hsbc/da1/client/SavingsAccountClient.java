package com.hsbc.da1.client;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountClient {

	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your option ==> ");
		System.out.println("1 => Array Backed");
		System.out.println("2 => ArrayList Backed");
		System.out.println("3 => JDBC Backed");
		System.out.println("3 => LinkedList Backed");
		
		
		System.out.println("======================================");
		
		int option = scanner.nextInt();
		scanner.close();
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getDao(option);
		SavingsAccountService savingsAccountService = SavingsAccountServiceFactory.getInstance(dao);
		SavingsAccountController accountController = new SavingsAccountController(savingsAccountService);
		
		//SavingsAccount azliSavingsAccount = accountController.openSavingsAccount("Azli", 25_000,"119/505","208012","Kanpur");
		//SavingsAccount inshalSavingsAccount = accountController.openSavingsAccount("Inshal", 56_000,"119/505","400123","Lucknow");
		
		SavingsAccount azliSavingsAccount = accountController.openSavingsAccount(110,"Azli", "azli@gmail.com",25_000);
		SavingsAccount inshalSavingsAccount = accountController.openSavingsAccount(112,"Inshal","inshal@gmail.com",56_000);
		SavingsAccount sairaSavingsAccount = accountController.openSavingsAccount(113,"saira", "saira@gmail.com",78_000);
				
		
		
		System.out.println("Account Id : "+ azliSavingsAccount.getAccountNumber()+", Customer name : "+ azliSavingsAccount.getCustomerName());
		System.out.println("Account Id : "+ inshalSavingsAccount.getAccountNumber()+", Customer name : "+ inshalSavingsAccount.getCustomerName());
		System.out.println("Account Id : "+ sairaSavingsAccount.getAccountNumber()+", Customer name : "+ sairaSavingsAccount.getCustomerName());
		
		
		System.out.println("-----------");
		try {
		accountController.transferAmount(inshalSavingsAccount.getAccountNumber(),azliSavingsAccount.getAccountNumber(), 10000);
		accountController.transferAmount(sairaSavingsAccount.getAccountNumber(), inshalSavingsAccount.getAccountNumber(), 10000);
		System.out.println(accountController.checkBalance(1001));
		System.out.println(accountController.checkBalance(1001));
		}
		catch(CustomerNotFound customerNotFound) {
			System.out.println(customerNotFound.getMessage());
		}
		catch(InsufficientBankBalance insufficientBalance) {
			System.out.println(insufficientBalance.getMessage());
		}
		System.out.println("-----------");
		
		
		
		Collection<SavingsAccount> savingsAccounts = accountController.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
				System.out.println("Account Balance : "+savingsAccount.getAccountBalance());
				
				
			}
		}
		
	}
}
