package com.hsbc.da1.exception;

public class InsufficientBankBalance extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientBankBalance(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return  super.getMessage();
	}

}
