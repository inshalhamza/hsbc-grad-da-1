package com.hsbc.da1.exception;

public class CustomerNotFound extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFound(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return  super.getMessage();
	}
}
