package com.hsbc.da1.dao;

import java.util.Collection;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO{
	SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);

	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);

	void deleteSavingsAccount(long accountNumber);

	Collection<SavingsAccount> fetchSavingsAccounts();

	SavingsAccount fetchSavingsAccountById(long accountNumber) throws CustomerNotFound;
	
}
