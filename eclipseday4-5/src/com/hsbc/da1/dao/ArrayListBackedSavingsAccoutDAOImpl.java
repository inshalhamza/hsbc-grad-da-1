package com.hsbc.da1.dao;

import java.util.ArrayList;

import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;


public class ArrayListBackedSavingsAccoutDAOImpl implements SavingsAccountDAO {
		private List<SavingsAccount> savingsAccountList= new ArrayList<>();
		
		
		public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
			savingsAccountList.add(savingsAccount);
			return savingsAccount;
			
		}
		
		
		public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount) {
			for (SavingsAccount sa : savingsAccountList) {
				if(sa.getAccountNumber() == accountNumber) {
					sa = savingsAccount;
				}
			}
			return savingsAccount;
		}
		
		public void deleteSavingsAccount(long accountNumber){
			for (SavingsAccount sa: savingsAccountList) {
				if (sa.getAccountNumber() ==accountNumber) {
					this.savingsAccountList.remove(sa);
				}
			}
			
		}
		
		public List<SavingsAccount> fetchSavingsAccounts() {
			return this.savingsAccountList;
			
		}
		
		public SavingsAccount fetchSavingsAccountById(long accountNumber) throws CustomerNotFound{
			
			for(SavingsAccount sa: savingsAccountList) {
				if (sa.getAccountNumber() == accountNumber) {
					return sa;
				}
			}
			throw new CustomerNotFound("Customer Does not exist");
			
		}
		

	}


