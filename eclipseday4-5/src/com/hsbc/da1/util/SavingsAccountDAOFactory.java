package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ArrayListBackedSavingsAccoutDAOImpl;

import com.hsbc.da1.dao.LinkedListBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {
	public static SavingsAccountDAO getDao(int value) {
		switch(value) {
		case 1: return new ArrayBackedSavingsAccountDAOImpl();
		case 2: return new ArrayListBackedSavingsAccoutDAOImpl();
		//case 3: return new JdbcBackedSavingsAccountDAOImpl();
		default: 
			return new LinkedListBackedSavingsAccountDAOImpl();
		}
	}

}
