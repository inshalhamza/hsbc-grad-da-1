package com.hsbc.da1.service;



import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountService {
	
	public SavingsAccount createSavingsAccount(int accountNumber,String customerName, double accountBalance ,String email);

	public void deleteSavingsAccount(long accountNumber);

	public double withdraw(long accountId, double amount) throws CustomerNotFound, InsufficientBankBalance;

	public double deposit(long accountId, double amount) throws CustomerNotFound ;

	public Collection<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound;

	public double checkBalance(long accountId) throws CustomerNotFound;

	public void transfer(long accountId, long toId, double amount)throws InsufficientBankBalance,CustomerNotFound ;
}
