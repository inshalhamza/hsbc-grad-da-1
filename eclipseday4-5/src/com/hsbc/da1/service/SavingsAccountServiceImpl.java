package com.hsbc.da1.service;

import java.util.Collection;
import java.util.List;


import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountServiceImpl implements SavingsAccountService {

	private SavingsAccountDAO saAccdao ;
	
	public SavingsAccountServiceImpl(SavingsAccountDAO dao) {
		this.saAccdao = dao;
	}

	public SavingsAccount createSavingsAccount(int accountNumber,String customerName, double accountBalance, String email) {
		
		//SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount savingsAccount = new SavingsAccount(accountNumber, customerName, email,accountBalance);
		SavingsAccount savingsAccountCreated = this.saAccdao.createSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public void deleteSavingsAccount(long accountNumber){
		 this.saAccdao.deleteSavingsAccount(accountNumber);
		
	}

	public double withdraw(long accountId, double amount) throws CustomerNotFound,InsufficientBankBalance {
		SavingsAccount savingsAccount = this.saAccdao.fetchSavingsAccountById(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if (currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.saAccdao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			}
			else {
				throw new InsufficientBankBalance("You Do Not Have Enough Bank Resources for transfer");
			}
		}
		return 0;
	}

	public double deposit(long accountId, double amount) throws CustomerNotFound{
		SavingsAccount savingsAccount = this.saAccdao.fetchSavingsAccountById(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.saAccdao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public Collection<SavingsAccount> fetchSavingsAccounts() {
		return this.saAccdao.fetchSavingsAccounts();
	
	}
	
	
	
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.saAccdao.fetchSavingsAccountById(accountNumber);		
		return savingsAccount;
		
	}
	
	public double checkBalance(long accountId) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.saAccdao.fetchSavingsAccountById(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public void transfer(long accountId, long toId, double amount) throws InsufficientBankBalance,CustomerNotFound {
		SavingsAccount fromAccount = this.saAccdao.fetchSavingsAccountById(accountId);
		SavingsAccount toAccount = this.saAccdao.fetchSavingsAccountById(toId);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if ( updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}
}
