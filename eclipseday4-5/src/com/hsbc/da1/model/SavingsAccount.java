package com.hsbc.da1.model;

public class SavingsAccount {
	private String customerName;
	private double accountBalance;
	private Address address;
	private String emailAddress;
	private int accountNumber;
	
	
	
	
	public int getAccountNumber() {
		return accountNumber;
	}



	public String getEmailAddress() {
		return emailAddress;
	}



	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}



	public SavingsAccount(String customerName,String emailAddress,double accountBalance,int zipCode, String streetName, String cityName) {
		Address address = new Address(zipCode,streetName,cityName);
		this.accountBalance = accountBalance;
		this.customerName = customerName;
		this.address = address;
		this.emailAddress = emailAddress;

	}
	
	public SavingsAccount(int accountNumber,String customerName,String emailAddress,double accountBalance) {
		this.accountBalance = accountBalance;
		this.customerName = customerName;
		this.emailAddress = emailAddress;
		this.accountNumber = accountNumber;

	}
	



	public Address getAddress() {
		return address;
	}

	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public double getAccountBalance() {
		return accountBalance;
	}
	
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
}
