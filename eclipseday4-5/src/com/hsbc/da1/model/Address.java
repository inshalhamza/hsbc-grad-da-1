package com.hsbc.da1.model;

public class Address {
	
		private int zipCode;
		private String streetName;	
		private String cityName;
		
		
		
		
		public Address(int zipCode, String streetName, String cityName) {
			this.zipCode = zipCode;
			this.streetName = streetName;
			this.cityName = cityName;
		}
		
		

		@Override
		public String toString() {
			return "Address [zipCode=" + zipCode + ", streetName=" + streetName + ", cityName=" + cityName + "]";
		}



		public int getZipCode() {
			return zipCode;
		}
		public void setZipCode(int zipCode) {
			this.zipCode = zipCode;
		}
		public String getStreetName() {
			return streetName;
		}
		public void setStreetName(String streetName) {
			this.streetName = streetName;
		}
		public String getCityName() {
			return cityName;
		}
		public void setCityName(String cityName) {
			this.cityName = cityName;
		}
	
		
		

}
