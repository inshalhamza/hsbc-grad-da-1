package com.hsbc.da1.controller;



import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
//import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {
	
	private SavingsAccountService savingsAccountService;
	
	public SavingsAccountController(SavingsAccountService savingsAccountService) {
		this.savingsAccountService = savingsAccountService;
	}
	
	public SavingsAccount openSavingsAccount(int accountNumber,String customerName,String email,double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(accountNumber,customerName, accountBalance,email);
		return savingsAccount;	
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}
	
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountService.fetchSavingsAccounts();
		
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountNumber,double amount) throws InsufficientBankBalance,CustomerNotFound  {
		double amountCr = this.savingsAccountService.withdraw(accountNumber, amount);
		return amountCr;
	}
	
	public double deposit(long accountId, double amount) throws CustomerNotFound {
		return this.savingsAccountService.deposit(accountId, amount);
	}
	
	public double checkBalance(long accountId) throws CustomerNotFound {
		return this.savingsAccountService.checkBalance(accountId);
	}
	
	public void transferAmount(long accountId,long accountIdTo,double amount) throws InsufficientBankBalance,CustomerNotFound{
		this.savingsAccountService.transfer(accountId, accountIdTo, amount);
	}
	

}
