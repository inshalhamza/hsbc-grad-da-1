package com.inshal.da1.threads;

public class ThreadDemo {
	public static void main(String[] args) {
		Thread t = Thread.currentThread();
		System.out.println("The name of thread --> "+t.getName());
		
		for(int i = 0 ; i < 10 ; i ++ ) {
			System.out.println("The name of threadId --> "+t.getId());
			System.out.println("Thread state --> "+t.getState().name());
			try {
				Thread.sleep(2000);
			}
			catch(InterruptedException e) {
				System.out.println("Thread Interrupted in sleep");
			}
		}
		}

	
}
