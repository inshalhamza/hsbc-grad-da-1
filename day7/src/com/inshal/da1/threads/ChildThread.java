package com.inshal.da1.threads;


public class ChildThread  extends Thread {
	
	public static void main(String[] args) {
		Thread.currentThread().setName("main");
		System.out.println(" Main Thread when the program is starting -> "+ Thread.currentThread().getName());
		
		//object initialization 
		ChildThread t1  = new ChildThread();
		t1.setName("child thread - 1 ");
		
		ChildThread t2  = new ChildThread();
		t2.setName("child thread - 2");
		
		ChildThread t3  = new ChildThread();
		t3.setName("child thread - 3");
		//Spawning a new thread
		t1.start();
		t2.start();
		t3.start();
		
		for ( int i = 0; i < 5; i ++) {
			System.out.println("Inside the main thread "+ Thread.currentThread().getName());
			try {
				Thread.currentThread();
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(" Main Thread is completed");
	}
	
	@Override
	public void run () {
		System.out.println("==================Thread " + Thread.currentThread().getName() + " start ==================");
		for ( int i = 0; i < 10; i ++) {

			System.out.println(" Name of the Thread => "+ Thread.currentThread().getName());
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("==================Thread " + Thread.currentThread().getName() + " terminates ==================");
	}

}
