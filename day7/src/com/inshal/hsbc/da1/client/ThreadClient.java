package com.inshal.hsbc.da1.client;


import com.inshal.hsbc.da1.runnableClasses.Flickr;
import com.inshal.hsbc.da1.runnableClasses.GooglePhotos;
import com.inshal.hsbc.da1.runnableClasses.Picassa;

public class ThreadClient extends Thread {
	public static void main(String[] args) {
		Thread.currentThread().setName("main");
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickrs = new Flickr();
		Runnable picassas = new Picassa();
		
		Thread googlePhoto = new Thread(googlePhotos);
		googlePhoto.setName("GooglePhotos");
		googlePhoto.setPriority(1);
		
		Thread flickr = new Thread(flickrs);
		flickr.setName("Flickr");
		flickr.setPriority(4);
		
		Thread picassa = new Thread(picassas);
		picassa.setName("Picassa");
		picassa.setPriority(1);

		picassa.start();
		googlePhoto.start();
		flickr.start();
		
		
		
			System.out.println("Inside the main thread -->"+ Thread.currentThread().getName());
			try {
				picassa.join();
				googlePhoto.join();
				flickr.join();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		finally {
		
		System.out.println("All Photos are downloaded from where ever possible");
		}
	}
}

