package com.inshal.da1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo {

	private static final String SELECT_QUERY = ("SELECT DISTINCT * FROM ITEMS ");
	public static void main(String[] args) {
		try (Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/MyDb", "admin", "password");
				Statement smt = conn.createStatement();) {
			int updatedQueries = smt.executeUpdate("INSERT INTO ITEMS (NAME,PRICE) VALUES('legion',125000),('Predator',215000)");
			System.out.println("Affected Querries: "+updatedQueries);
			ResultSet result = smt.executeQuery(SELECT_QUERY);
			while(result.next()) {
				System.out.println("Items :"+result.getString(1)+" Price: "+result.getString(2));
			}
		} catch (SQLException e) {
			// TODO: handle exception
		}
	}
}
